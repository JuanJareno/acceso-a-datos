package com.juanjareno.practicaad1.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.juanjareno.practicaad1.base.Lampara;


import javax.swing.*;

public class Ventana {

    //DECLARAMOS LOS BOTONES, RADIO BUTTON, DATE PICKER Y LIST
    public JPanel panel1;
    public JRadioButton HalogenaRadioButton;
    public JRadioButton LEDRadioButton;
    public JTextField codigoBarrasTxt;
    public JTextField marcaTxt;
    public JTextField modeloTxt;
    public JTextField lumenesvoltiosTxt;
    public JButton nuevoBtn;
    public JButton exportarBtn;
    public JButton importarBtn;

    public JList lista;
    public DatePicker fechaCompraDPicker;
    public JFrame frame;
    public DefaultListModel<Lampara> dlmLampara;


    private JRadioButton halogenaRadioButton;
    private JButton ImportarButton;
    private JButton NuevoButton;
    private JButton ExportarButton;
    private JTextField marcatxt;
    private JTextField modelotxt;
    private JTextField codigoBarrastxt;
    private JTextField voltioslumenestxt;


    public Ventana() {
        frame = new JFrame("LamparaPracticaAD1");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        initComponents();
    }

    private void initComponents(){
        dlmLampara = new DefaultListModel<Lampara>();
        lista.setModel(dlmLampara);
    }

}
