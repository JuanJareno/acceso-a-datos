package gui;

import base.enums.TipoLamparas;
import base.enums.TipoEnvio;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Vista extends JFrame {
    private final static String TITULOFRAME="Aplicación MVC Juan Jareño";
    private JTabbedPane tabbedPane;
    private JPanel panel1;
    private JPanel JPanelLampara;
    private JPanel JPanelFabricante;
    private JPanel JPanelTienda;

    //LAMPARAS
    JTextField txtNombre;
    JComboBox comboFabricante;
    JComboBox comboTienda;
    JComboBox comboTipoLamparas;
    DatePicker fecha;
    JTextField txtCodigobarras;
    JTextField txtPrecioLamparas;
    JTable lamparasTabla;
    JButton anadirLampara;
    JButton modificarLampara;
    JButton eliminarLampara;

    //FABRICANTE
    JTextField txtNombreFabricante;
    JTextField txtDireccion;
    DatePicker fechaCreacion;
    JTextField txtPais;
    JTable fabricanteTabla;
    JButton eliminarFabricante;
    JButton anadirFabricante;
    JButton modificarFabricante;

    //TIENDA
    JTextField txtNombreTienda;
    JTextField txtEmail;
    JTextField txtTelefono;
    JComboBox comboTipoEnvio;
    JTextField txtWeb;
    JTable tiendaTabla;
    JButton eliminarTienda;
    JButton anadirTienda;
    JButton modificarTienda;

    //BUSQUEDA
    JLabel etiquetaEstado;

    //DEFAULT TABLE MODEL
    DefaultTableModel dtmTienda;
    DefaultTableModel dtmFabricante;
    DefaultTableModel dtmLamparas;

    //BARRA MENU
    JMenuItem itemOpciones;
    JMenuItem itemDesconectar;
    JMenuItem itemSalir;


    //CUADRO DIALOGO
    OptionDialog optionDialog;
    JDialog adminPasswordDialog;
    JButton btnValidate;
    JPasswordField adminPassword;

    public Vista() {
        super(TITULOFRAME);
        initFrame();
    }

    public void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth()+250,this.getHeight()+150));
        this.setLocationRelativeTo(this);
        //creo cuadro de dialogo
        optionDialog=new OptionDialog(this);
        setMenu();
        setAdminDialog();
        setEnumComboBox();
        setTableModels();
    }

    private void setTableModels() {
        this.dtmLamparas =new DefaultTableModel();
        this.lamparasTabla.setModel(dtmLamparas);
        this.dtmFabricante =new DefaultTableModel();
        this.fabricanteTabla.setModel(dtmFabricante);
        this.dtmTienda=new DefaultTableModel();
        this.tiendaTabla.setModel(dtmTienda);
    }

    private void setMenu() {
        JMenuBar mbBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        //por cada item que tenga funcionalidad tiene un ActionCommand
        itemOpciones= new JMenuItem("Opciones");
        itemOpciones.setActionCommand("Opciones");
        itemDesconectar= new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");
        itemSalir=new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");
        menu.add(itemOpciones);
        menu.add(itemDesconectar);
        menu.add(itemSalir);
        mbBar.add(menu);
        //centrar en horizontal
        mbBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(mbBar);
    }

    private void setEnumComboBox() {
        for (TipoEnvio constant: TipoEnvio.values()) {
            comboTipoEnvio.addItem(constant.getValor());
        }
        comboTipoEnvio.setSelectedIndex(-1);
        for (TipoLamparas constant: TipoLamparas.values()) {
            comboTipoLamparas.addItem(constant.getValor());
        }
        comboTipoLamparas.setSelectedIndex(-1);
    }

    private void setAdminDialog() {
        btnValidate= new JButton("Validar");
        btnValidate.setActionCommand("abrirOpciones");
        adminPassword= new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(100,26));
        Object[] options = new Object[] {adminPassword,btnValidate};
        JOptionPane jop =new JOptionPane("Introduce la contraseña",
                JOptionPane.WARNING_MESSAGE,JOptionPane.YES_NO_OPTION,null,options);
        adminPasswordDialog=new JDialog(this,"Opciones",true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);
    }

}
