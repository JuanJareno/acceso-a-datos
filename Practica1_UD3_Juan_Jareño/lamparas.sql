CREATE DATABASE if not exists lamparas;

USE lamparas;

create table if not exists fabricantes(
idfabricante int auto_increment primary key,
nombre varchar(50) not null,
direccion varchar(50) not null,
fechacreacion date,
pais varchar(50));

create table if not exists tiendas(
idtienda int auto_increment primary key,
tienda varchar(50) not null,
email varchar(50) not null,
telefono varchar(9),
tipoenvio varchar(50),
web varchar(500));

create table if not exists compradores(
idcomprador int auto_increment primary key,
nombre varchar(50) not null,
direccion varchar(50) not null,
telefono varchar(9),
email varchar(50),
fechacompra date);

create table if not exists lamparas(
idlampara int auto_increment primary key,
nombre varchar(50) not null,
codigobarras varchar(50) not null UNIQUE,
idtienda int not null,
tipolamparas varchar(50),
idfabricante int not null,
precio float not null,
fechafabricacion date);

create table if not exists fabricante_tienda(
idfabricante INT UNSIGNED REFERENCES fabricantes,
idtienda INT UNSIGNED REFERENCES tiendas,
PRIMARY KEY (idtienda, idfabricante));

create table if not exists compradores_tienda(
idcomprador INT UNSIGNED REFERENCES compradores,
idtienda INT UNSIGNED REFERENCES tiendas,
fechacompra date,
PRIMARY KEY (idcomprador, idtienda));

create table if not exists lamparas_fabricante(
idlampara INT UNSIGNED REFERENCES lamparas,
idfabricante INT UNSIGNED REFERENCES fabricantes,
precio double,
fechafabricacion date,
PRIMARY KEY (idlampara, idfabricante));