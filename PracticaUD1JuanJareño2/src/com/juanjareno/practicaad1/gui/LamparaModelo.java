package com.juanjareno.practicaad1.gui;

import com.juanjareno.practicaad1.base.Halogena;
import com.juanjareno.practicaad1.base.LED;
import com.juanjareno.practicaad1.base.Lampara;
import jdk.internal.org.xml.sax.SAXException;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class LamparaModelo {

        private ArrayList<Lampara> listaLampara;

        public LamparaModelo() {
            listaLampara = new ArrayList<Lampara>();
        }

        public ArrayList<Lampara> obtenerLampara() {
            return listaLampara;
        }

        //DAMOS DE ALTA HALOGENA Y LED
        public void altaHalogena(String codigoBarras, String marca, String modelo,
                                 LocalDate fechaCompra, int voltios) {
            Halogena nuevoHalogena = new Halogena(codigoBarras, marca, modelo,
                    fechaCompra, voltios);
            listaLampara.add(nuevoHalogena);
        }


        public void altaLED(String codigoBarras, String marca, String modelo,
                            LocalDate fechaCompra, double lumenes) {
            LED nuevaLED = new LED(codigoBarras, marca, modelo,
                    fechaCompra, lumenes);
            listaLampara.add(nuevaLED);
        }

        //METODO QUE COMPRUEBA SI EXISTE EL CODIGO DE BARRAS

        public boolean existecodigoBarras(String codigoBarras) {
            for (Lampara unaLampara : listaLampara) {
                if (unaLampara.getcodigoBarras().equals(codigoBarras)) {
                    return true;
                }
            }
            return false;
        }


    public boolean eliminarRecambio(String nombre) {
        for (Lampara lampara : listaLampara) {
            if (lampara.getcodigoBarras().equals(nombre)) {
                listaLampara.remove(lampara);
                return true;
            }
        }
        return false;
    }

        //METODO QUE CREA EL EXPORTAR

        public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();
            Document documento = dom.createDocument(null, "xml", null);

            Element raiz = documento.createElement("Lamparas");
            documento.getDocumentElement().appendChild(raiz);
            Element nodoLampara = null;
            Element nodoDatos = null;
            Text texto = null;

            for (Lampara unaLampara : listaLampara) {
                //AÑADOR DENTRO LAMPARA DEPENDIENDO DE SI ES LED O HALOGENA
                if (unaLampara instanceof Halogena) {
                    nodoLampara = documento.createElement("Halogena");
                } else {
                    nodoLampara = documento.createElement("LED");
                }
                raiz.appendChild(nodoLampara);


                nodoDatos = documento.createElement("codigo de barras");
                nodoLampara.appendChild(nodoDatos);
                texto = documento.createTextNode(unaLampara.getcodigoBarras());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("marca");
                nodoLampara.appendChild(nodoDatos);
                texto = documento.createTextNode(unaLampara.getMarca());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("modelo");
                nodoLampara.appendChild(nodoDatos);
                texto = documento.createTextNode(unaLampara.getModelo());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("fecha-compra");
                nodoLampara.appendChild(nodoDatos);
                texto = documento.createTextNode(unaLampara.getFechaCompra().toString());
                nodoDatos.appendChild(texto);

                if (unaLampara instanceof Halogena) {
                    nodoDatos = documento.createElement("voltios");
                    nodoLampara.appendChild(nodoDatos);
                    texto = documento.createTextNode(String.valueOf(((Halogena) unaLampara).getVoltios()));
                    nodoDatos.appendChild(texto);
                } else {
                    nodoDatos = documento.createElement("lumenes");
                    nodoLampara.appendChild(nodoDatos);
                    texto = documento.createTextNode(String.valueOf(((LED) unaLampara).getLumenes()));
                    nodoDatos.appendChild(texto);

                    Source source = new DOMSource(documento);
                    Result resultado = new StreamResult(fichero);

                    Transformer transformer = TransformerFactory.newInstance().newTransformer();
                    transformer.transform(source, resultado);
                }

            }
        }

        //METODO QUE CREA EL IMPORTAR
        public void importarXML(File fichero) throws ParserConfigurationException, IOException, SAXException, org.xml.sax.SAXException {
            listaLampara = new ArrayList<Lampara>();
            Halogena nuevoHalogena = null;
            LED nuevaLED = null;

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document documento = builder.parse(fichero);

            NodeList listaElementos = documento.getElementsByTagName("*");

            for (int i = 0; i < listaElementos.getLength(); i++) {
                Element nodoLampara = (Element) listaElementos.item(i);

                if (nodoLampara.getTagName().equals("Halogena")) {
                    nuevoHalogena = new Halogena();
                    nuevoHalogena.setcodigoBarras(nodoLampara.getChildNodes().item(0).getTextContent());
                    nuevoHalogena.setMarca(nodoLampara.getChildNodes().item(1).getTextContent());
                    nuevoHalogena.setModelo(nodoLampara.getChildNodes().item(2).getTextContent());
                    nuevoHalogena.setFechaCompra(LocalDate.parse(nodoLampara.getChildNodes().item(3).getTextContent()));
                    nuevoHalogena.setVoltios(Integer.parseInt(nodoLampara.getChildNodes().item(4).getTextContent()));
                    listaLampara.add(nuevoHalogena);
                } else {
                    if (nodoLampara.getTagName().equals("LED")) {
                        nuevaLED = new LED();
                        nuevaLED.setcodigoBarras(nodoLampara.getChildNodes().item(0).getTextContent());
                        nuevaLED.setMarca(nodoLampara.getChildNodes().item(1).getTextContent());
                        nuevaLED.setModelo(nodoLampara.getChildNodes().item(2).getTextContent());
                        nuevaLED.setFechaCompra(LocalDate.parse(nodoLampara.getChildNodes().item(3).getTextContent()));
                        nuevaLED.setLumenes(Double.parseDouble(nodoLampara.getChildNodes().item(4).getTextContent()));
                        listaLampara.add(nuevaLED);
                    }
                }

            }

        }
    }




