package com.practicaUD4.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.practicaUD4.base.Lampara;
import com.practicaUD4.base.Fabricante;

import javax.swing.*;

/**
 * Clase Vista
 * @author
 */
public class Vista {
    //Campos
    private JPanel panel1;

    //LAMPARAS
    JTextField txtNombre;
    JTextField txtPrecio;
    JTextField txtBuscar;
    DatePicker dateLampara;
    JList listLampara;
    JButton btnNuevo;
    JButton btnModificar;
    JButton btnEliminar;
    JTextField txtMarca;

    //FABRICANTE
    private JTabbedPane tabbedPane1;
    JTextField txtNombreFabricante;
    JTextField txtMarcaFabricante;
    JTextField txtBuscarFabricante;
    JTextField txtPrecioFabricante;
    JList listFabricante;
    DatePicker dateFabricante;
    JButton btnNuevoFabricante;
    JButton btnModificarFabricante;
    JButton btnEliminarFabricante;

    DefaultListModel<Lampara> dlmLampara;
    DefaultListModel<Fabricante> dlmFabricante;

    /**
     * Constructor de Vista()
     */
    public Vista() {
        JFrame frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        inicializar();
    }

    /**
     * inicializar(),inicializar modelos
     */
    private void inicializar(){
        dlmLampara = new DefaultListModel<Lampara>();
        listLampara.setModel(dlmLampara);

        dlmFabricante = new DefaultListModel<Fabricante>();
        listFabricante.setModel(dlmFabricante);

        dateLampara.getComponentToggleCalendarButton().setText("Fecha");
        dateFabricante.getComponentToggleCalendarButton().setText("Fecha");
    }
}
