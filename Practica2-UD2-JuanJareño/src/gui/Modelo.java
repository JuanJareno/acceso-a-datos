package gui;

import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.Properties;

/**
 * Created by DAM on 13/12/2021.
 */
public class Modelo {
    private String ip;
    private String user;
    private String password;
    private String adminPassword;

    public String getIp() {
        return ip;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public Modelo() {
        getPropValues();
    }

    private Connection conexion;

    void conectar() {
        try {
            conexion = DriverManager.getConnection("jdbc:mysql://"
                    + ip + ":3306/aplicacionud2", user, password);
        } catch (SQLException e) {
            try {
                conexion = DriverManager.getConnection("jdbc:mysql://"
                        + ip + ":3306/", user, password);
                PreparedStatement statement = null;

                String code = leerFichero();
                String[] query = code.split("--");
                for (String aQuery : query) {
                    statement = conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement != null;
                statement.close();
            } catch (SQLException | IOException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }
    }

    private String leerFichero() throws IOException {
        //basedatos_java no tiene delimitador
        //StringBuilder es dinamica
        try (BufferedReader reader = new BufferedReader(new FileReader("basedatos_java.sql"))) {
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }
            return stringBuilder.toString();
        }
    }

    void desconectar() {
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    void insertarFabricante(String nombre, String direccion, LocalDate fechaCreacion, String pais) {
        String sentenciaSql = "INSERT INTO fabricantes (nombre, direccion, fechacreacion, pais)" +
                "VALUES (?,?,?,?)";

        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, direccion);
            sentencia.setDate(3, Date.valueOf(fechaCreacion));
            sentencia.setString(4, pais);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    void insertarTienda(String tienda, String email, String telefono, String tipoEnvio, String web) {
        String sentenciaSql = "INSERT INTO tiendas (tienda, email, telefono, tipoenvio, web) VALUES (?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, tienda);
            sentencia.setString(2, email);
            sentencia.setString(3, telefono);
            sentencia.setString(4, tipoEnvio);
            sentencia.setString(5, web);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void insertarLampara(String nombre, String codigobarras, String tienda, String tipolamparas, String fabricante,
                         float precio, LocalDate fechaFabricacion) {
        String sentenciaSql = "INSERT INTO lamparas (nombre, codigobarras, idtienda, tipolamparas, idfabricante, precio, fechafabricacion) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        int idtienda = Integer.valueOf(tienda.split(" ")[0]);
        int idfabricante = Integer.valueOf(fabricante.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, codigobarras);
            sentencia.setInt(3, idtienda);
            sentencia.setString(4, tipolamparas);
            sentencia.setInt(5, idfabricante);
            sentencia.setFloat(6, precio);
            sentencia.setDate(7, Date.valueOf(fechaFabricacion));
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void modificarFabricante(String nombre, String direccion, LocalDate fechacreacion, String pais, int idfabricante) {
        String sentenciaSql = "UPDATE fabricantes SET nombre=?,direccion=?,fechacreacion=?,pais=?" +
                "WHERE idfabricante=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, direccion);
            sentencia.setDate(3, Date.valueOf(fechacreacion));
            sentencia.setString(4, pais);
            sentencia.setInt(5, idfabricante);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void modificarTienda(String tienda, String email, String telefono, String tipoEnvio, String web, int idtienda) {

        String sentenciaSql = "UPDATE tiendas SET tienda = ?, email = ?, telefono = ?, tipoenvio = ?, web = ?" +
                "WHERE idtienda = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, tienda);
            sentencia.setString(2, email);
            sentencia.setString(3, telefono);
            sentencia.setString(4, tipoEnvio);
            sentencia.setString(5, web);
            sentencia.setInt(6, idtienda);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void modificarLampara(String nombre, String codigobarras, String tienda, String tipolamparas, String fabricante,
                        float precio, LocalDate fechafabricacion, int idlampara) {

        String sentenciaSql = "UPDATE lamparas SET nombre = ?, codigobarras = ?, idtienda = ?, tipolamparas = ?, " +
                "idfabricante = ?, precio = ?, fechafabricacion = ? WHERE idlampara = ?";
        PreparedStatement sentencia = null;

        int idtienda = Integer.valueOf(tienda.split(" ")[0]);
        int idfabricante = Integer.valueOf(fabricante.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, codigobarras);
            sentencia.setInt(3, idtienda);
            sentencia.setString(4, tipolamparas);
            sentencia.setInt(5, idfabricante);
            sentencia.setFloat(6, precio);
            sentencia.setDate(7, Date.valueOf(fechafabricacion));
            sentencia.setInt(8, idlampara);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void borrarTienda(int idtienda) {
        String sentenciaSql = "DELETE FROM tiendas WHERE idtienda=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idtienda);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void borrarFabricante(int idfabricante) {
        String sentenciaSql = "DELETE FROM fabricantes WHERE idfabricante = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idfabricante);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void borrarLampara(int idlampara) {
        String sentenciaSql = "DELETE FROM lamparas WHERE idlampara = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idlampara);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    ResultSet consultarTienda() throws SQLException {
        String sentenciaSql = "SELECT concat(idtienda) AS 'ID', concat(tienda) AS 'Nombre tienda', " +
                "concat(email) AS 'email', concat(telefono) AS 'Teléfono',concat(tipoenvio) AS 'Tipo'," +
                "concat(web) AS 'Web' FROM tiendas";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    ResultSet consultarFabricante() throws SQLException {
        String sentenciaSql = "SELECT concat(idfabricante) AS 'ID', concat(nombre) AS 'Nombre', concat(direccion) AS 'Direccion', " +
                "concat(fechacreacion) AS 'Fecha de creacion', concat(pais) AS 'País de origen' FROM fabricantes";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    ResultSet consultarLamparas() throws SQLException {
        String sentenciaSql = "SELECT concat(b.idlampara) AS 'ID', concat(b.nombre) AS 'Nombre', concat(b.codigobarras) AS 'Codigo de barras', " +
                "concat(e.idtienda, ' - ', e.tienda) AS 'Tienda', concat(b.tipolamparas) AS 'Tipo de lamparas', " +
                "concat(a.idfabricante, ' - ', a.direccion, ', ', a.nombre) AS 'Fabricante', " +
                "concat(b.precio) AS 'Precio', concat(b.fechafabricacion) AS 'Fecha de fabricacion'" +
                " FROM lamparas AS b " +
                "INNER JOIN tiendas AS e ON e.idtienda = b.idtienda INNER JOIN " +
                "fabricantes AS a ON a.idfabricante = b.idfabricante";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    //usamos los datos del cuadro de dialogo
    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";
            inputStream = new FileInputStream(propFileName);
            prop.load(inputStream);
            ip = prop.getProperty("ip");
            user = prop.getProperty("user");
            password = prop.getProperty("pass");
            adminPassword = prop.getProperty("admin");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null)
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

    void setPropValues(String ip, String user, String pass, String adminPass) {
        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", user);
            prop.setProperty("pass", pass);
            prop.setProperty("admin", adminPass);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.ip=ip;
        this.user=user;
        this.password=pass;
        this.adminPassword=adminPass;
    }

    //comprobaciones llamando a funciones de sql
    public boolean lamparaCodigobarrasYaExiste(String codigobarras) {
        String consulta="SELECT existecodigoBarras(?)";
        PreparedStatement function;
        boolean codigobarrasExists=false;
        try {
            function=conexion.prepareStatement(consulta);
            function.setString(1,codigobarras);
            ResultSet rs =function.executeQuery();
            rs.next();
            codigobarrasExists=rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return codigobarrasExists;
    }

    public boolean tiendaNombreYaExiste(String nombre) {
        String tiendaNameConsult = "SELECT existeNombreTienda(?)";
        PreparedStatement function;
        boolean nameExists = false;
        try {
            function = conexion.prepareStatement(tiendaNameConsult);
            function.setString(1, nombre);
            ResultSet rs = function.executeQuery();
            rs.next();

            nameExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameExists;
    }
    public boolean fabricanteNombreYaExiste(String nombre, String direccion) {
        String completeName = direccion + ", " + nombre;
        String fabricanteNameConsult = "SELECT existeNombreFabricante(?)";
        PreparedStatement function;
        boolean nameExists = false;
        try {
            function = conexion.prepareStatement(fabricanteNameConsult);
            function.setString(1, completeName);
            ResultSet rs = function.executeQuery();
            rs.next();

            nameExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameExists;
    }


}
