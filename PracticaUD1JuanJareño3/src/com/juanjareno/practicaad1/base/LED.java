package com.juanjareno.practicaad1.base;

import java.time.LocalDate;

public class LED extends Lampara {

    private int lumenes;

    public LED(){
        super();
    }

    public LED(String tamaño, String marca, String modelo, LocalDate fechaCompra, int lumenes) {
        super(tamaño, marca, modelo, fechaCompra);
        this.lumenes = lumenes;
    }

    public int getLumenes() {
        return lumenes;
    }

    public void setLumenes(int lumenes) {
        this.lumenes = lumenes;
    }

    @Override
    public String toString(){
        return "LED{"
                +"modelo=" + getModelo()
                +"marca=" + getMarca()
                +"codigo de barras " + getcodigoBarras()
                +"Fecha compra " + getFechaCompra()
                +'}';

    }
}
