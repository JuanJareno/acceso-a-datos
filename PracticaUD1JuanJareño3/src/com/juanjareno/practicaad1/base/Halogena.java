package com.juanjareno.practicaad1.base;

import java.time.LocalDate;

public class Halogena extends Lampara{

    private int voltios;

    public Halogena() {super();}

    public Halogena(String codigoBarras, String marca, String modelo, LocalDate fechaCompra, int voltios) {
        super(codigoBarras, marca, modelo, fechaCompra);
        this.voltios = voltios;
    }

    public int getVoltios() {
        return voltios;
    }

    public void setVoltios(int voltios) {
        this.voltios = voltios;
    }

    @Override

    public String toString(){
        return "Halogena{"
                +"codigo de barras" + getcodigoBarras()
                +"marca"  + getMarca()
                +"modelo=" + getModelo()
                +"fecha_compra= "+getFechaCompra()
                +'}';
    }

}
