package com.juanjareno.practicaad1.gui;

import com.juanjareno.practicaad1.base.Halogena;
import com.juanjareno.practicaad1.base.LED;
import com.juanjareno.practicaad1.base.Lampara;
import com.juanjareno.practicaad1.util.Util;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;
//IMPLEMENTAMOS LOS LISTENER
public class LamparaControlador implements ActionListener, ListSelectionListener, WindowListener {

    private Ventana vista;
    private LamparaModelo modelo;
    private File ultimaRutaExportada;

    public LamparaControlador(Ventana vista, LamparaModelo modelo){
        this.vista=vista;
        this.modelo=modelo;

        try{
            cargarDatosConfiguracion();
        }catch (IOException e) {
            System.out.println("No existe el fichero de configuracion " + e.getMessage());

        }

        //AÑADIMOS LOS LISTENER

        addActionListener(this);
        addListSelectionListener(this);
        addWindowListener(this);
    }

    //CREAMOS LOS METODOS DE DATOS
    private void cargarDatosConfiguracion() throws  IOException{
        Properties configuracion = new Properties();
        configuracion.load(new FileReader("lampara.conf"));
        ultimaRutaExportada=new File(configuracion.getProperty("ultimaRutaExportada"));
    }

    private void actualizarDatosConfiguracion(File ultimaRutaExportada){
        this.ultimaRutaExportada = ultimaRutaExportada;
    }

    private void guardarDatosConfiguracion() throws IOException{
        Properties configuracion = new Properties();
        configuracion.setProperty("ultimaRutaExportada", ultimaRutaExportada.getAbsolutePath());

        configuracion.store(new PrintWriter("vehiculos.conf"),
        "Datos configuuracion vehiculos");
    }

    @Override
    public void actionPerformed(ActionEvent e){
        String actionCommand = e.getActionCommand();

        //IMPLEMENTAMOS LOS BOTONES
        switch(actionCommand){
            case "Nuevo":
                if(hayCamposVacios()){
                    Util.mensajeError("Los siguientes campos no pueden estar vacios \n" +
                            "codigoBarras \n Marca \n Modelo \n fechaCompra\n" +
                            vista.lumenesvoltiosTxt.getText());
                    break;
                }

                if(modelo.existecodigoBarras(vista.codigoBarrasTxt.getText())){
                    Util.mensajeError("Ya existe una lampara con este codigo de barras \n" +
                            vista.lumenesvoltiosTxt.getText());
                    break;
                }

               if(vista.HalogenaRadioButton.isSelected()){
                   modelo.altaHalogena(vista.codigoBarrasTxt.getText(),
                           vista.marcaTxt.getText(),
                           vista.modeloTxt.getText(),
                           vista.fechaCompraDPicker.getDate(),
                           Integer.parseInt(vista.lumenesvoltiosTxt.getText()));
               }
               limpiarCampos();
               refrescar();
               break;

            case"Importar":
                JFileChooser selectorFichero =
                        Util.crearSelectorFichero(ultimaRutaExportada,
                                "Archivos XML", "xml");
                int opt=selectorFichero.showOpenDialog(null);
                if(opt==JFileChooser.APPROVE_OPTION){
                    try{
                        modelo.importarXML(selectorFichero.getSelectedFile());
                    }catch(ParserConfigurationException e1){
                        e1.printStackTrace();
                    }catch (IOException e1){
                        e1.printStackTrace();
                    }catch (jdk.internal.org.xml.sax.SAXException ex){
                        ex.printStackTrace();
                    } catch (SAXException e1) {
                        e1.printStackTrace();
                    }
                    refrescar();
                }
                break;

            case"Exportar":
                JFileChooser selectorFichero2 =
                        Util.crearSelectorFichero(ultimaRutaExportada,"Archivos XML", "xml");
                int opt2=selectorFichero2.showSaveDialog(null);
                if(opt2==JFileChooser.APPROVE_OPTION){
                    try{
                        modelo.exportarXML(selectorFichero2.getSelectedFile());
                        actualizarDatosConfiguracion(selectorFichero2.getSelectedFile());
                    }catch (ParserConfigurationException e1){
                        e1.printStackTrace();
                    }catch (TransformerException e1){
                        e1.printStackTrace();
                    }
                }
                break;
            case"LED":
                vista.lumenesvoltiosTxt.setText("lumenes");
                break;

            case"Halogena":
                vista.lumenesvoltiosTxt.setText("voltios");
                break;
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e){
        if(e.getValueIsAdjusting()){
            Lampara lamparaSeleccionado = (Lampara) vista.lista.getSelectedValue();
            vista.codigoBarrasTxt.setText(lamparaSeleccionado.getcodigoBarras());
            vista.marcaTxt.setText(lamparaSeleccionado.getMarca());
            vista.modeloTxt.setText(lamparaSeleccionado.getModelo());
            vista.fechaCompraDPicker.setDate(lamparaSeleccionado.getFechaCompra());

            if(lamparaSeleccionado instanceof Halogena){
                vista.HalogenaRadioButton.doClick();
                vista.lumenesvoltiosTxt.setText(String.valueOf(((Halogena) lamparaSeleccionado).getVoltios()));
            }else{
                vista.LEDRadioButton.doClick();
                vista.lumenesvoltiosTxt.setText(String.valueOf(((LED) lamparaSeleccionado).getLumenes()));
            }
        }
    }

    //AÑADIMOS A LOS BUTTON Y RADIOBUTTON EL LISTENER
    private void addActionListener(ActionListener listener){
        vista.HalogenaRadioButton.addActionListener(listener);
        vista.LEDRadioButton.addActionListener(listener);
        vista.exportarBtn.addActionListener(listener);
        vista.importarBtn.addActionListener(listener);
        vista.nuevoBtn.addActionListener(listener);
    }
    //LISTENER DEL FRAME
    private void addWindowListener(WindowListener listener){ vista.frame.addWindowListener(listener);}

    //LISTENER DE LA LISTA
    private void addListSelectionListener(ListSelectionListener listener){
        vista.lista.addListSelectionListener(listener);
    }
    //CREAMOS METODO LIMPIAR CAMPOS
    private void limpiarCampos(){
        vista.lumenesvoltiosTxt.setText(null);
        vista.marcaTxt.setText(null);
        vista.codigoBarrasTxt.setText(null);
        vista.modeloTxt.setText(null);
        vista.fechaCompraDPicker.setText(null);
        vista.codigoBarrasTxt.requestFocus();

    }

    //CREAMOS METODO COMPROBAR CAMPOS VACIOS

    private boolean hayCamposVacios() {
        if (vista.lumenesvoltiosTxt.getText().isEmpty() ||
                vista.marcaTxt.getText().isEmpty() ||
                vista.codigoBarrasTxt.getText().isEmpty() ||
                vista.modeloTxt.getText().isEmpty() ||
                vista.fechaCompraDPicker.getText().isEmpty()) {
            return true;
        }
        return false;
    }

        //METODO PARA CARGAR DATOS DENTRO DE UNA LISTA
        public void refrescar(){
            vista.dlmLampara.clear();
            for(Lampara unaLampara : modelo.obtenerLampara()){
                vista.dlmLampara.addElement(unaLampara);
            }
        }

        //METODO PARA CERRAR LA VENTANA
        @Override
                public void windowClosing(WindowEvent e){
        int resp = Util.mensajeConfirmacion("¿Desea cerrar la ventana?","Salir");
        if(resp == JOptionPane.NO_OPTION){
            try{
                guardarDatosConfiguracion();
            }catch (IOException e1){
                e1.printStackTrace();
            }
            System.exit(0);
        }
    }

    @Override
    public void windowClosed(WindowEvent e){

    }
    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }


    @Override
    public void windowOpened(WindowEvent e) {

    }

}
