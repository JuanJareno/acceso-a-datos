package com.practicaUD4.base;

import org.bson.types.ObjectId;

import java.time.LocalDate;


public class Fabricante {
    //Campos
    private ObjectId id;
    private String nombre;
    private String marca;
    private double precioFabricante;
    private LocalDate fechaFabricacion;


    public Fabricante() {
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }


    public double getPrecioFabricante() {
        return precioFabricante;
    }

    public void setPrecioFabricante(double precioFabricante) {
        this.precioFabricante = precioFabricante;
    }


    public LocalDate getFechaFabricacion() {
        return fechaFabricacion;
    }

    public void setFechaFabricacion(LocalDate fechaFabricacion) {
        this.fechaFabricacion = fechaFabricacion;
    }


    @Override
    public String toString() {
        return  nombre + ":" + marca;
    }
}
