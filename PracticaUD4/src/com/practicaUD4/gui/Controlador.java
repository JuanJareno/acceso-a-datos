package com.practicaUD4.gui;

import com.practicaUD4.base.Fabricante;
import com.practicaUD4.base.Lampara;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

public class Controlador implements ActionListener, KeyListener, ListSelectionListener {

        Vista vista;
        Modelo modelo;


    public Controlador(Vista vista, Modelo modelo) {
            this.vista = vista;
            this.modelo = modelo;

            inicializar();
        }

        private void inicializar() {
        addActionListeners(this);
        addKeyListeners(this);
        addListSelectionListeners(this);
        modelo.conectar();
        listarLamparas(modelo.getLamparas());
        listarFabricantes(modelo.getFabricantes());
    }

    private void addActionListeners(ActionListener listener){
        vista.btnNuevo.addActionListener(listener);
        vista.btnModificar.addActionListener(listener);
        vista.btnEliminar.addActionListener(listener);
        vista.btnEliminarFabricante.addActionListener(listener);
        vista.btnModificarFabricante.addActionListener(listener);
        vista.btnNuevoFabricante.addActionListener(listener);

    }

    /**
     * Método addListSelectionListeners(), añadir listeners a los JLists
     * @param listener de ListSelectionListener
     */
    private void addListSelectionListeners(ListSelectionListener listener){
        vista.listLampara.addListSelectionListener(listener);
        vista.listFabricante.addListSelectionListener(listener);
    }

    /**
     * Método addKeyListeners(), añadir listeners a los campos de buscar
     * @param listener de KeyListener
     */
    private void addKeyListeners(KeyListener listener){
        vista.txtBuscar.addKeyListener(listener);
        vista.txtBuscarFabricante.addKeyListener(listener);
    }

    /**
     * Método actionPerformed(), implementado por ActionListener, donde asignas las acciones a los botones
     * @param e de ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        Lampara unaLampara;
        Fabricante unFabricante;
        switch (comando){
            case "Nuevo":
                unaLampara = new Lampara();
                modificarLamparaFromCampos(unaLampara);
                modelo.guardarLampara(unaLampara);
                listarLamparas(modelo.getLamparas());

                break;
            case "Modificar":
                unaLampara = (Lampara) vista.listLampara.getSelectedValue();
                modificarLamparaFromCampos(unaLampara);
                modelo.modificarLampara(unaLampara);
                listarLamparas(modelo.getLamparas());
                break;
            case "Eliminar":
                unaLampara = (Lampara) vista.listLampara.getSelectedValue();
                modelo.borrarLampara(unaLampara);
                listarLamparas(modelo.getLamparas());
                break;

            case "Nuevo Pescado":
                unFabricante = new Fabricante();
                modificarFabricanteFromCampos(unFabricante);
                modelo.guardarFabricante(unFabricante);
                listarFabricantes(modelo.getFabricantes());
                break;
            case "Modificar Pescado":
                unFabricante = (Fabricante) vista.listFabricante.getSelectedValue();
                modificarFabricanteFromCampos(unFabricante);
                modelo.modificarFabricante(unFabricante);

                listarFabricantes(modelo.getFabricantes());
                break;
            case "Eliminar Pescado":
                unFabricante = (Fabricante) vista.listFabricante.getSelectedValue();
                modelo.borrarFabricante(unFabricante);
                listarFabricantes(modelo.getFabricantes());
                break;
        }
    }

    private void listarLamparas(List<Lampara> lista){
        vista.dlmLampara.clear();
        for (Lampara lampara : lista){
            vista.dlmLampara.addElement(lampara);
        }
    }

    private void listarFabricantes(List<Fabricante> lista){
        vista.dlmFabricante.clear();
        for (Fabricante fabricante : lista){
            vista.dlmFabricante.addElement(fabricante);
        }
    }

    private void modificarLamparaFromCampos(Lampara unaLampara) {
        unaLampara.setNombre(vista.txtNombre.getText());
        unaLampara.setMarca(vista.txtMarca.getText());
        unaLampara.setPrecio(Double.parseDouble(vista.txtPrecio.getText()));
        unaLampara.setFechaCompra(vista.dateLampara.getDate());
    }

    private void modificarFabricanteFromCampos(Fabricante unFabricante) {
        unFabricante.setNombre(vista.txtNombreFabricante.getText());
        unFabricante.setMarca(vista.txtMarcaFabricante.getText());
        unFabricante.setPrecioFabricante(Double.parseDouble(vista.txtPrecioFabricante.getText()));
        unFabricante.setFechaFabricacion(vista.dateFabricante.getDate());
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()){
            Lampara unaLampara = (Lampara) vista.listLampara.getSelectedValue();
            vista.txtNombre.setText(unaLampara.getNombre());
            vista.txtMarca.setText(unaLampara.getMarca());
            vista.txtPrecio.setText(String.valueOf(unaLampara.getPrecio()));
            vista.dateLampara.setDate(unaLampara.getFechaCompra());
        }
        if(e.getValueIsAdjusting()){
            Fabricante unFabricante = (Fabricante) vista.listFabricante.getSelectedValue();
            vista.txtNombreFabricante.setText(unFabricante.getNombre());
            vista.txtMarcaFabricante.setText(unFabricante.getMarca());
            vista.txtPrecioFabricante.setText(String.valueOf(unFabricante.getPrecioFabricante()));
            vista.dateLampara.setDate(unFabricante.getFechaFabricacion());
        }
    }


    @Override
    public void keyReleased(KeyEvent e) {
        if(e.getSource() == vista.txtBuscar){
            listarLamparas(modelo.getLamparas(vista.txtBuscar.getText()));
        }
        if(e.getSource() == vista.txtBuscarFabricante){
            listarFabricantes(modelo.getFabricante(vista.txtBuscarFabricante.getText()));
        }
    }
    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }




}
