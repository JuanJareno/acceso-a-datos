package com.juanjareno.practicaad1.gui;

import com.juanjareno.practicaad1.base.Lampara;
import com.juanjareno.practicaad1.base.Halogena;
import com.juanjareno.practicaad1.base.LED;
//import com.juanjareno.practicaad1.gui.Ventana.Ventana;
import com.juanjareno.practicaad1.util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;




import java.io.*;
import java.util.Properties;

public class LamparaControlador implements ActionListener, ListSelectionListener, WindowListener {

    private Ventana vista;
    private LamparaModelo modelo;
    private File ultimaRutaExportada;

    public LamparaControlador(Ventana vista, LamparaModelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        try {
            cargarDatosConfiguracion();
        } catch (IOException e) {
            System.out.println("No existe el fichero de configuracion " + e.getMessage());
        }

        //AÑADIMOS LOS LISTENER

        addActionListener(this);
        addListSelectionListener(this);
        addWindowListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();

        //IMPLEMENTAMOS LOS BOTONES
        switch (actionCommand) {
            case "Nuevo":
                if (hayCamposVacios()) {
                    Util.mensajeError("Los siguientes campos no pueden estar vacios \n" +
                            "codigoBarras \n Marca \n Modelo \n fechaCompra\n" +
                            vista.voltioslumenestxt.getText());
                    break;
                }

                if (modelo.existecodigoBarras(vista.codigoBarrastxt.getText())) {
                    Util.mensajeError("Ya existe una lampara con este codigo de barras");
                    vista.voltioslumenestxt.getText();
                    break;
                }

                if (vista.halogenaRadioButton.isSelected()) {
                    modelo.altaHalogena(vista.codigoBarrastxt.getText(),
                            vista.marcatxt.getText(),
                            vista.modelotxt.getText(),
                            vista.fechaCompraDPicker.getDate(),
                            Integer.parseInt(vista.voltioslumenestxt.getText()));
                } else {
                    modelo.altaLED(vista.codigoBarrastxt.getText(),
                            vista.marcatxt.getText(),
                            vista.modelotxt.getText(),
                            vista.fechaCompraDPicker.getDate(),
                            Integer.parseInt(vista.voltioslumenestxt.getText()));
                }
                limpiarCampos();
                refrescar();
                break;

            case "Importar":
                JFileChooser selectorFichero =
                        Util.crearSelectorFichero(ultimaRutaExportada,
                                "Archivo XML", "xml");
                int opt = selectorFichero.showOpenDialog(null);
                if (opt == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.importarXML(selectorFichero.getSelectedFile());
                    } catch (ParserConfigurationException e1) {
                        e1.printStackTrace();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    } catch (org.xml.sax.SAXException e1) {
                        e1.printStackTrace();
                    }

                    refrescar();
                }
                break;

            case "Exportar":
                JFileChooser selectorFichero2 = Util.crearSelectorFichero(ultimaRutaExportada, "Archivo XML", "xml");
                int opt2 = selectorFichero2.showSaveDialog(null);
                if (opt2 == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.exportarXML(selectorFichero2.getSelectedFile());
                        actualizarDatosConfiguracion(selectorFichero2.getSelectedFile());
                    } catch (ParserConfigurationException e1) {
                        e1.printStackTrace();
                    } catch (TransformerException e1) {
                        e1.printStackTrace();
                    }
                }
                break;

            case "LED":
                vista.lblvoltioslumenes.setText("Lumenes");
                break;

            case "Halogena":
                vista.lblvoltioslumenes.setText("Voltios");
                break;

            case "Eliminar":
                if (!modelo.eliminarRecambio(vista.codigoBarrastxt.getText())) {
                    Util.mensajeError("No se ha podido borrar ");
                } else {
                    limpiarCampos();
                    refrescar();
                    Util.mensajeExitoso("Eliminado correctamente");
                }
                limpiarCampos();
                refrescar();
                break;
        }
    }

    private boolean hayCamposVacios() {
        if (vista.voltioslumenestxt.getText().isEmpty() ||
                vista.marcatxt.getText().isEmpty() ||
                vista.codigoBarrastxt.getText().isEmpty() ||
                vista.modelotxt.getText().isEmpty() ||
                vista.fechaCompraDPicker.getText().isEmpty()) {
            return true;
        }
        return false;
    }

    private void limpiarCampos() {
        vista.voltioslumenestxt.setText(null);
        vista.marcatxt.setText(null);
        vista.codigoBarrastxt.setText(null);
        vista.modelotxt.setText(null);
        vista.fechaCompraDPicker.setText(null);
        vista.codigoBarrastxt.requestFocus();

    }

    public void refrescar() {
        vista.dlmLampara.clear();
        for (Lampara unaLampara : modelo.obtenerLampara()) {
            vista.dlmLampara.addElement(unaLampara);
        }
    }

    private void addActionListener(ActionListener listener) {
        vista.halogenaRadioButton.addActionListener(listener);
        vista.LEDRadioButton.addActionListener(listener);
        vista.nuevoButton.addActionListener(listener);
        vista.importarButton.addActionListener(listener);
        vista.exportarButton.addActionListener(listener);
        vista.eliminarButton.addActionListener(listener);
    }

    private void addWindowListener(WindowListener listener) {
        vista.frame.addWindowListener(listener);
    }

    private void addListSelectionListener(ListSelectionListener listener) {
        vista.lista.addListSelectionListener(listener);
    }

    private void cargarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.load(new FileReader("lampara.conf"));
        if(configuracion.getProperty("ultimaRutaExportada") !=null){
        ultimaRutaExportada = new File(configuracion.getProperty("ultimaRutaExportada"));
    }

}
    private void actualizarDatosConfiguracion(File ultimaRutaExportada){
        this.ultimaRutaExportada = ultimaRutaExportada;
    }



    private void guardarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        if (ultimaRutaExportada == null) {
            ultimaRutaExportada = new File(System.getProperty("user.dir"));
        }
        configuracion.setProperty("ultimaRutaExportada", ultimaRutaExportada.getAbsolutePath());
        configuracion.store(new PrintWriter("lampara.conf"), "Datos configuracion lampara");
    }


    @Override
    public void valueChanged(ListSelectionEvent e){
        if(e.getValueIsAdjusting()){
            Lampara lamparaSeleccionado = (Lampara) vista.lista.getSelectedValue();
            vista.codigoBarrastxt.setText(lamparaSeleccionado.getcodigoBarras());
            vista.marcatxt.setText(lamparaSeleccionado.getMarca());
            vista.modelotxt.setText(lamparaSeleccionado.getModelo());
            vista.fechaCompraDPicker.setDate(lamparaSeleccionado.getFechaCompra());

            if(lamparaSeleccionado instanceof Halogena){
                vista.halogenaRadioButton.doClick();
                vista.voltioslumenestxt.setText(String.valueOf(((Halogena) lamparaSeleccionado).getVoltios()));
            }else{
                vista.LEDRadioButton.doClick();
                vista.voltioslumenestxt.setText(String.valueOf(((LED) lamparaSeleccionado).getLumenes()));
            }
        }
    }


    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e){
        int resp = Util.mensajeConfirmacion("¿Desea cerrar la ventana?","Salir");
        if(resp == JOptionPane.YES_OPTION){
            vista.frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            try{
                guardarDatosConfiguracion();
            }catch (IOException e1){
                e1.printStackTrace();
            }
            vista.frame.dispose();
        }else if(resp == JOptionPane.NO_OPTION){
            vista.frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        }


    }


    @Override
    public void windowClosed(WindowEvent e){

    }
    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }




}

