CREATE DATABASE if not exists aplicacionlamparas;

USE aplicacionlamparas;

create table if not exists fabricantes(
                                          idfabricante int auto_increment primary key,
                                          nombre varchar(50) not null,
                                          direccion varchar(50) not null,
                                          fechacreacion date,
                                          pais varchar(50));

create table if not exists tiendas(
                                      idtienda int auto_increment primary key,
                                      tienda varchar(50) not null,
                                      email varchar(50) not null,
                                      telefono varchar(9),
                                      tipolampara varchar(50),
                                      web varchar(500));

create table if not exists lamparas(
                                       idlampara int auto_increment primary key,
                                       nombre varchar(50) not null,
                                       codigobarras varchar(50) not null UNIQUE,
                                       idtienda int not null,
                                       marca int not null,
                                       tipo varchar(50),
                                       idfabricante int not null,
                                       precio float not null,
                                       fechafabricacion date);

alter table lamparas
    add foreign key (idtienda) references tiendas(idtienda),
    add foreign key(idfabricante) references fabricantes(idfabricante);


create function existecodigoBarras(f_codigoBarras varchar(40))
    returns bit
begin
    declare i int;
    set i = 0;
    while ( i < (select max(idlampara) from lamparas)) do
            if  ((select codigoBarras from lamparas where idlampara = (i + 1)) like f_codigoBarras) then return 1;
            end if;
            set i = i + 1;
        end while;
    return 0;
end;



create function existeNombreTienda(f_name varchar(50))
    returns bit
begin
    declare i int;
    set i=0;
    while(i<(select max(idtienda) from tiendas)) do
            if ((select tienda from tiendas where idtienda = (i+1)) like f_name) then return 1;
            end if;
            set i = i+1;
        end while;
    return 0;
end;



create function existeNombreFabricante(f_name varchar(202))
    returns bit
begin
    declare i int;
    set i=0;
    while (i<(select max(idfabricante) from fabricantes)) do
            if((select concat(direccion,',', nombre) from fabricantes where idfabricante= (i+1)) like f_name) then return 1;
            end if;
            set i=i+1;
        end while;
    return 0;
end;
