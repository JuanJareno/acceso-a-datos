package com.juanjareno.practicaad1.base;

import java.time.LocalDate;

public class LED extends Lampara {

 private double lumenes;

 public LED(){
     super();
 }

    public LED(String tamaño, String marca, String modelo, LocalDate fechaCompra, double lumenes) {
        super(tamaño, marca, modelo, fechaCompra);
        this.lumenes = lumenes;
    }

    public double getLumenes() {
        return lumenes;
    }

    public void setLumenes(double lumenes) {
        this.lumenes = lumenes;
    }

    @Override
    public String toString(){
     return "LED{"
             +"modelo=" + getModelo()
             +"marca=" + getMarca()
             +"codigo de barras " + getcodigoBarras() +
             "Lumenes" + lumenes+
             '}';

    }
}
