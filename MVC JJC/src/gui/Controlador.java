package gui;

import util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

/**
 * Created by DAM on 13/12/2021.
 */
public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener {
    private Modelo modelo;
    private Vista vista;
    boolean refrescar;

    public Controlador(Vista vista, Modelo modelo) {
        this.modelo = modelo;
        this.vista = vista;
        modelo.conectar();
        setOptions();
        addActionListeners(this);
        addItemListeners(this);
        addWindowListeners(this);
        refrescarTodo();
    }

    private void refrescarTodo() {
        refrescarFabricantes();
        refrescarTienda();
        refrescarLamparas();
        refrescar = false;
    }

    private void addActionListeners(ActionListener listener) {
        vista.anadirLampara.addActionListener(listener);
        vista.anadirLampara.setActionCommand("anadirLampara");
        vista.anadirFabricante.addActionListener(listener);
        vista.anadirFabricante.setActionCommand("anadirFabricante");
        vista.anadirTienda.addActionListener(listener);
        vista.anadirTienda.setActionCommand("anadirTienda");
        vista.eliminarLampara.addActionListener(listener);
        vista.eliminarLampara.setActionCommand("eliminarLampara");
        vista.eliminarFabricante.addActionListener(listener);
        vista.eliminarFabricante.setActionCommand("eliminarFabricante");
        vista.eliminarTienda.addActionListener(listener);
        vista.eliminarTienda.setActionCommand("eliminarTienda");
        vista.modificarLampara.addActionListener(listener);
        vista.modificarLampara.setActionCommand("modificarLampara");
        vista.modificarFabricante.addActionListener(listener);
        vista.modificarFabricante.setActionCommand("modificarFabricante");
        vista.modificarTienda.addActionListener(listener);
        vista.modificarTienda.setActionCommand("modificarTienda");
        vista.optionDialog.btnOpcionesGuardar.addActionListener(listener);
        vista.itemOpciones.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.btnValidate.addActionListener(listener);
    }

    private void addWindowListeners(WindowListener listener) {
        vista.addWindowListener(listener);
    }

    private void addItemListeners(Controlador controlador) {
    }

    /**
     * Muestra los atributos de un objeto seleccionado y los borra una vez se deselecciona
     *
     * @param e Evento producido en una lista
     */

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()
                && !((ListSelectionModel) e.getSource()).isSelectionEmpty()) {
            if (e.getSource().equals(vista.tiendaTabla.getSelectionModel())) {
                int row = vista.tiendaTabla.getSelectedRow();
                vista.txtNombreTienda.setText(String.valueOf(vista.tiendaTabla.getValueAt(row, 1)));
                vista.txtEmail.setText(String.valueOf(vista.tiendaTabla.getValueAt(row, 2)));
                vista.txtTelefono.setText(String.valueOf(vista.tiendaTabla.getValueAt(row, 3)));
                vista.comboTipoEnvio.setSelectedItem(String.valueOf(vista.tiendaTabla.getValueAt(row, 4)));
                vista.txtWeb.setText(String.valueOf(vista.tiendaTabla.getValueAt(row, 5)));
            } else if (e.getSource().equals(vista.fabricanteTabla.getSelectionModel())) {
                int row = vista.fabricanteTabla.getSelectedRow();
                vista.txtNombreFabricante.setText(String.valueOf(vista.fabricanteTabla.getValueAt(row, 1)));
                vista.txtDireccion.setText(String.valueOf(vista.fabricanteTabla.getValueAt(row, 2)));
                vista.fechaCreacion.setDate((Date.valueOf(String.valueOf(vista.fabricanteTabla.getValueAt(row, 3)))).toLocalDate());
                vista.txtPais.setText(String.valueOf(vista.fabricanteTabla.getValueAt(row, 4)));
            } else if (e.getSource().equals(vista.lamparasTabla.getSelectionModel())) {
                int row = vista.lamparasTabla.getSelectedRow();
                vista.txtNombre.setText(String.valueOf(vista.lamparasTabla.getValueAt(row, 1)));
                vista.comboFabricante.setSelectedItem(String.valueOf(vista.lamparasTabla.getValueAt(row, 5)));
                vista.comboTienda.setSelectedItem(String.valueOf(vista.lamparasTabla.getValueAt(row, 3)));
                vista.comboTipoLamparas.setSelectedItem(String.valueOf(vista.lamparasTabla.getValueAt(row, 4)));
                vista.fecha.setDate((Date.valueOf(String.valueOf(vista.lamparasTabla.getValueAt(row, 7)))).toLocalDate());
                vista.txtCodigobarras.setText(String.valueOf(vista.lamparasTabla.getValueAt(row, 2)));
                vista.txtPrecioLamparas.setText(String.valueOf(vista.lamparasTabla.getValueAt(row, 6)));
            } else if (e.getValueIsAdjusting()
                    && ((ListSelectionModel) e.getSource()).isSelectionEmpty() && !refrescar) {
                if (e.getSource().equals(vista.tiendaTabla.getSelectionModel())) {
                    borrarCamposTiendas();
                } else if (e.getSource().equals(vista.fabricanteTabla.getSelectionModel())) {
                    borrarCamposFabricantes();
                } else if (e.getSource().equals(vista.lamparasTabla.getSelectionModel())) {
                    borrarCamposLamparas();
                }
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {
            case "Opciones":
                vista.adminPasswordDialog.setVisible(true);
                break;
            case "Desconectar":
                modelo.desconectar();
                break;
            case "Salir":
                System.exit(0);
                break;
            case "abrirOpciones":
                if (String.valueOf(vista.adminPassword.getPassword()).equals(modelo.getAdminPassword())) {
                    vista.adminPassword.setText("");
                    vista.adminPasswordDialog.dispose();
                    vista.optionDialog.setVisible(true);
                } else {
                    Util.showErrorAlert("La contraseña introducida no es correcta");
                }
                break;
            case "guardarOpciones":
                modelo.setPropValues(vista.optionDialog.tfIP.getText(),
                        vista.optionDialog.tfUser.getText(),
                        String.valueOf(vista.optionDialog.pfPass.getPassword()),
                        String.valueOf(vista.optionDialog.pfAdmin.getPassword()));
                vista.optionDialog.dispose();
                vista.dispose();
                new Controlador(new Vista(), new Modelo());
                break;
            case "anadirLampara":
                try {
                    if (comprobarLamparaVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.lamparasTabla.clearSelection();
                    } else if (modelo.lamparaCodigobarrasYaExiste(vista.txtCodigobarras.getText())) {
                        Util.showErrorAlert("Ese codigo de barras ya existe");
                        vista.lamparasTabla.clearSelection();
                    } else {
                        modelo.insertarLampara(
                                vista.txtNombre.getText(),
                                vista.txtCodigobarras.getText(),
                                String.valueOf(vista.comboTienda.getSelectedItem()),
                                String.valueOf(vista.comboTipoLamparas.getSelectedItem()),
                                String.valueOf(vista.comboFabricante.getSelectedItem()),
                                Float.parseFloat(vista.txtPrecioLamparas.getText()),
                                vista.fecha.getDate());
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
                    vista.lamparasTabla.clearSelection();
                }
                borrarCamposLamparas();
                refrescarLamparas();
                break;
            case "modificarLampara":
                try {
                    if (comprobarLamparaVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.lamparasTabla.clearSelection();
                    } else {
                        modelo.modificarLampara(
                                vista.txtNombre.getText(),
                                vista.txtCodigobarras.getText(),
                                String.valueOf(vista.comboTienda.getSelectedItem()),
                                String.valueOf(vista.comboTipoLamparas.getSelectedItem()),
                                String.valueOf(vista.comboFabricante.getSelectedItem()),
                                Float.parseFloat(vista.txtPrecioLamparas.getText()),
                                vista.fecha.getDate(),
                                Integer.parseInt((String) vista.lamparasTabla.getValueAt(vista.lamparasTabla.getSelectedRow(), 0))
                        );
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
                    vista.lamparasTabla.clearSelection();
                }
                borrarCamposLamparas();
                refrescarLamparas();
                break;
            case "eliminarLampara":
                modelo.borrarLampara(Integer.parseInt((String) vista.lamparasTabla.getValueAt(vista.lamparasTabla.getSelectedRow(), 0)));
                borrarCamposLamparas();
                refrescarLamparas();
                break;
            case "anadirFabricante": {
                try {
                    if (comprobarAutorVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.fabricanteTabla.clearSelection();
                    } else if (modelo.fabricanteNombreYaExiste(vista.txtNombreFabricante.getText(),
                            vista.txtDireccion.getText())) {
                        Util.showErrorAlert("Ese nombre ya existe.\nIntroduce un autor diferente");
                        vista.fabricanteTabla.clearSelection();
                    } else {
                        modelo.insertarFabricante(vista.txtNombreFabricante.getText(),
                                vista.txtDireccion.getText(),
                                vista.fechaCreacion.getDate(),
                                vista.txtPais.getText());
                        refrescarFabricantes();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.fabricanteTabla.clearSelection();
                }
                borrarCamposFabricantes();
            }
            break;
            case "modificarFabricante": {
                try {
                    if (comprobarAutorVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.fabricanteTabla.clearSelection();
                    } else {
                        modelo.modificarFabricante(vista.txtNombreFabricante.getText(), vista.txtDireccion.getText(),
                                vista.fechaCreacion.getDate(), vista.txtPais.getText(),
                                Integer.parseInt((String) vista.fabricanteTabla.getValueAt(vista.fabricanteTabla.getSelectedRow(), 0)));
                        refrescarFabricantes();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.fabricanteTabla.clearSelection();
                }
                borrarCamposFabricantes();
            }
            break;
            case "eliminarFabricante":
                modelo.borrarFabricante(Integer.parseInt((String) vista.fabricanteTabla.getValueAt(vista.fabricanteTabla.getSelectedRow(), 0)));
                borrarCamposFabricantes();
                refrescarFabricantes();
                break;
            case "anadirTienda": {
                try {
                    if (comprobarEditorialVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tiendaTabla.clearSelection();
                    } else if (modelo.tiendaNombreYaExiste(vista.txtNombreTienda.getText())) {
                        Util.showErrorAlert("Ese nombre ya existe.\nIntroduce una editorial diferente.");
                        vista.tiendaTabla.clearSelection();
                    } else {
                        modelo.insertarTienda(vista.txtNombreTienda.getText(), vista.txtEmail.getText(),
                                vista.txtTelefono.getText(),
                                (String) vista.comboTipoEnvio.getSelectedItem(),
                                vista.txtWeb.getText());
                        refrescarTienda();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tiendaTabla.clearSelection();
                }
                borrarCamposTiendas();
            }
            break;
            case "modificarTienda": {
                try {
                    if (comprobarEditorialVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tiendaTabla.clearSelection();
                    } else {
                        modelo.modificarTienda(vista.txtNombreTienda.getText(), vista.txtEmail.getText(), vista.txtTelefono.getText(),
                                String.valueOf(vista.comboTipoEnvio.getSelectedItem()), vista.txtWeb.getText(),
                                Integer.parseInt((String) vista.tiendaTabla.getValueAt(vista.tiendaTabla.getSelectedRow(), 0)));
                        refrescarTienda();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tiendaTabla.clearSelection();
                }
                borrarCamposTiendas();
            }
            break;
            case "eliminarTienda":
                modelo.borrarTienda(Integer.parseInt((String) vista.tiendaTabla.getValueAt(vista.tiendaTabla.getSelectedRow(), 0)));
                borrarCamposTiendas();
                refrescarTienda();
                break;
        }
    }

    private void refrescarTienda() {
        try {
            vista.tiendaTabla.setModel(construirTableModelEditoriales(modelo.consultarTienda()));
            vista.comboTienda.removeAllItems();
            for (int i = 0; i < vista.dtmTienda.getRowCount(); i++) {
                vista.comboTienda.addItem(vista.dtmTienda.getValueAt(i, 0) + " - " +
                        vista.dtmTienda.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModelEditoriales(ResultSet rs)
            throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmTienda.setDataVector(data, columnNames);
        return vista.dtmTienda;
    }

    private void refrescarFabricantes() {
        try {
            vista.fabricanteTabla.setModel(construirTableModeloFabricante(modelo.consultarFabricante()));
            vista.comboFabricante.removeAllItems();
            for (int i = 0; i < vista.dtmFabricante.getRowCount(); i++) {
                vista.comboFabricante.addItem(vista.dtmFabricante.getValueAt(i, 0) + " - " +
                        vista.dtmFabricante.getValueAt(i, 2) + ", " + vista.dtmFabricante.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModeloFabricante(ResultSet rs)
            throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmFabricante.setDataVector(data, columnNames);
        return vista.dtmFabricante;
    }

    /**
     * Actualiza los libros que se ven en la lista y los comboboxes
     */
    private void refrescarLamparas() {
        try {
            vista.lamparasTabla.setModel(construirTableModelLibros(modelo.consultarLamparas()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModelLibros(ResultSet rs)
            throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }
        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        vista.dtmLamparas.setDataVector(data, columnNames);
        return vista.dtmLamparas;

    }

    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

    private void setOptions() {
    }

    private void borrarCamposLamparas() {
        vista.comboTienda.setSelectedIndex(-1);
        vista.comboFabricante.setSelectedIndex(-1);
        vista.txtNombre.setText("");
        vista.txtCodigobarras.setText("");
        vista.comboTipoLamparas.setSelectedIndex(-1);
        vista.txtPrecioLamparas.setText("");
        vista.fecha.setText("");
    }

    private void borrarCamposFabricantes() {
        vista.txtNombreFabricante.setText("");
        vista.txtDireccion.setText("");
        vista.txtPais.setText("");
        vista.fechaCreacion.setText("");
    }

    private void borrarCamposTiendas() {
        vista.txtNombreTienda.setText("");
        vista.txtEmail.setText("");
        vista.txtTelefono.setText("");
        vista.comboTipoEnvio.setSelectedIndex(-1);
        vista.txtWeb.setText("");
    }

    private boolean comprobarLamparaVacio() {
        return vista.txtNombre.getText().isEmpty() ||
                vista.txtPrecioLamparas.getText().isEmpty() ||
                vista.txtCodigobarras.getText().isEmpty() ||
                vista.comboTipoLamparas.getSelectedIndex() == -1 ||
                vista.comboFabricante.getSelectedIndex() == -1 ||
                vista.comboTienda.getSelectedIndex() == -1 ||
                vista.fecha.getText().isEmpty();
    }

    private boolean comprobarAutorVacio() {
        return vista.txtDireccion.getText().isEmpty() ||
                vista.txtNombreFabricante.getText().isEmpty() ||
                vista.txtPais.getText().isEmpty() ||
                vista.fechaCreacion.getText().isEmpty();
    }

    private boolean comprobarEditorialVacia() {
        return vista.txtNombreTienda.getText().isEmpty() ||
                vista.txtEmail.getText().isEmpty() ||
                vista.txtTelefono.getText().isEmpty() ||
                vista.comboTipoEnvio.getSelectedIndex() == -1 ||
                vista.txtWeb.getText().isEmpty();
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
