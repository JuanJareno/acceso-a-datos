package base.enums;

public enum TipoLamparas {

    LAMPARAINCANDESCENTES("Incandescentes"),
    LAMPARASHALOGENAS("Halogenas"),
    LAMPARASFLUORESCENTES("Fluorescentes"),
    LAMPARASLED("Led");

    private String valor;
    TipoLamparas(String valor){ this.valor = valor;}
    public String getValor() {return valor;}
}

