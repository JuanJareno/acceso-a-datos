package com.juanjareno.practicaad1.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.juanjareno.practicaad1.base.Lampara;


import javax.swing.*;

public class Ventana {

    //DECLARAMOS LOS BOTONES, RADIO BUTTON, DATE PICKER Y LIST
    public JPanel panel1;
    public JRadioButton LEDRadioButton;
    public JRadioButton halogenaRadioButton;
    public JButton importarButton;
    public JButton nuevoButton;
    public JButton exportarButton;
    public JButton eliminarButton;
    public JTextField marcatxt;
    public JTextField modelotxt;
    public JTextField codigoBarrastxt;
    public JTextField voltioslumenestxt;
    public JLabel lblvoltioslumenes;

    public JList lista;
    public DatePicker fechaCompraDPicker;
    public JFrame frame;
    public DefaultListModel<Lampara> dlmLampara;


    public Ventana() {
        frame = new JFrame("LamparaPracticaAD1");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        halogenaRadioButton.setSelected(true);


        initComponents();
    }

    private void initComponents(){
        dlmLampara = new DefaultListModel<Lampara>();
        lista.setModel(dlmLampara);
    }

}

