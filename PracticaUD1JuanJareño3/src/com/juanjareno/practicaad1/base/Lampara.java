package com.juanjareno.practicaad1.base;

import java.time.LocalDate;

public class Lampara {

    private String codigoBarras;
    private String marca;
    private String modelo;
    private LocalDate fechaCompra;

    public Lampara(){

    }

    public Lampara(String codigoBarras, String marca, String modelo, LocalDate fechaCompra){
        this.codigoBarras = codigoBarras;
        this.marca = marca;
        this.modelo = modelo;
        this.fechaCompra = fechaCompra;
    }

    public String getcodigoBarras() {
        return codigoBarras;
    }

    public void setcodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public LocalDate getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(LocalDate fechaCompra) {
        this.fechaCompra = fechaCompra;
    }
}
