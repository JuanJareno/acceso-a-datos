package com.juanjareno.practicaad1;

import com.juanjareno.practicaad1.gui.LamparaControlador;
import com.juanjareno.practicaad1.gui.LamparaModelo;
import com.juanjareno.practicaad1.gui.Ventana;

public class Principal {
    public static void main(String[] args) {
        //Declaro la vista , modelo y controlador para visualizar el programa
        Ventana vista= new Ventana();
        LamparaModelo modelo=new LamparaModelo();
        LamparaControlador controlador = new LamparaControlador(vista,modelo);
    }
}
