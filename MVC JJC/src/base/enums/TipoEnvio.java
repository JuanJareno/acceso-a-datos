package base.enums;

public enum TipoEnvio {
    ENVIONORMAL("Normal"),
    RAPIDO("Rapido"),
    PERSONALIZADO("Personalizado"),
    PUNTOENTREGA("Punto de entrega"),
    INTERNACIONAL("Internacional");

    private String valor;
    TipoEnvio(String valor){ this.valor = valor;}
    public String getValor() {return valor;}

}
