package com.practicaUD4.gui;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.practicaUD4.base.Fabricante;
import com.practicaUD4.base.Lampara;
import com.practicaUD4.util.Util;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Clase Modelo
 * @author
 */
public class Modelo {

    //Campos
    private final static String COLECCION_LAMPARAS = "Lamparas";
    private final static String COLECCION_FABRICANTES = "Fabricantes";
    private final static String DATABASE = "Lamparas";

    private MongoClient client;
    private MongoDatabase baseDatos;
    private MongoCollection coleccionlamparas;
    private MongoCollection coleccionfabricantes;


    public void conectar() {
        client = new MongoClient();
        baseDatos = client.getDatabase(DATABASE);
        coleccionlamparas = baseDatos.getCollection(COLECCION_LAMPARAS);
        coleccionfabricantes =baseDatos.getCollection(COLECCION_FABRICANTES);
    }


    public void desconectar(){
        client.close();

    }

    public void guardarLampara(Lampara unaLampara) {
        coleccionlamparas.insertOne(lamparaToDocument(unaLampara));

    }


    public void guardarFabricante(Fabricante unFabricante) {
        coleccionfabricantes.insertOne(fabricanteToDocument(unFabricante));

    }


    public List<Lampara> getLamparas(){
        ArrayList<Lampara> lista = new ArrayList<>();

        Iterator<Document> it = coleccionlamparas.find().iterator();
        while(it.hasNext()){
            lista.add(documentToLampara(it.next()));
        }
        return lista;
    }


    public List<Fabricante> getFabricantes(){
        ArrayList<Fabricante> lista = new ArrayList<>();

        Iterator<Document> it = coleccionfabricantes.find().iterator();
        while(it.hasNext()){
            lista.add(documentToFabricante(it.next()));
        }
        return lista;
    }



    public List<Lampara> getLamparas(String text) {
        ArrayList<Lampara> lista = new ArrayList<>();

        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("nombre", new Document("$regex", "/*"+text+"/*")));
        listaCriterios.add(new Document("marca", new Document("$regex", "/*"+text+"/*")));
        query.append("$or", listaCriterios);

        Iterator<Document> iterator = coleccionlamparas.find(query).iterator();
        while(iterator.hasNext()){
            lista.add(documentToLampara(iterator.next()));
        }

        return lista;
    }

    public List<Fabricante> getFabricante(String text) {
        ArrayList<Fabricante> lista = new ArrayList<>();

        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("nombre", new Document("$regex", "/*"+text+"/*")));
        listaCriterios.add(new Document("marca", new Document("$regex", "/*"+text+"/*")));
        query.append("$or", listaCriterios);

        Iterator<Document> iterator = coleccionfabricantes.find(query).iterator();
        while(iterator.hasNext()){
            lista.add(documentToFabricante(iterator.next()));
        }

        return lista;
    }


    public Document lamparaToDocument(Lampara unaLampara){
        Document documento = new Document();
        documento.append("nombre", unaLampara.getNombre());
        documento.append("marca", unaLampara.getMarca());
        documento.append("precio", unaLampara.getPrecio());
        documento.append("fechaCompra", Util.formatearFecha(unaLampara.getFechaCompra()));
        return documento;
    }

    public Document fabricanteToDocument(Fabricante unFabricante){
        Document documento = new Document();
        documento.append("nombre", unFabricante.getNombre());
        documento.append("marca", unFabricante.getMarca());
        documento.append("precioFabricante", unFabricante.getPrecioFabricante());
        documento.append("fechaFabricacion", Util.formatearFecha(unFabricante.getFechaFabricacion()));
        return documento;
    }


    public Lampara documentToLampara(Document document){
        Lampara unaLampara = new Lampara();
        unaLampara.setId(document.getObjectId("_id"));
        unaLampara.setNombre(document.getString("nombre"));
        unaLampara.setMarca(document.getString("marca"));
        unaLampara.setPrecio(document.getDouble("precio"));
        unaLampara.setFechaCompra(Util.parsearFecha(document.getString("fechaCompra")));
        return unaLampara;
    }

    public Fabricante documentToFabricante(Document document){
        Fabricante unFabricante = new Fabricante();
        unFabricante.setId(document.getObjectId("_id"));
        unFabricante.setNombre(document.getString("nombre"));
        unFabricante.setMarca(document.getString("marca"));
        unFabricante.setPrecioFabricante(document.getDouble("precioFabricante"));
        unFabricante.setFechaFabricacion(Util.parsearFecha(document.getString("fechaFabricacion")));
        return unFabricante;
    }


    public void modificarLampara(Lampara unaLampara) {
        coleccionlamparas.replaceOne(new Document("_id", unaLampara.getId()), lamparaToDocument(unaLampara));
    }

    public void modificarFabricante(Fabricante unFabricante) {
        coleccionfabricantes.replaceOne(new Document("_id", unFabricante.getId()), fabricanteToDocument(unFabricante));
    }


    public void borrarLampara(Lampara unaLampara)
    {
        coleccionlamparas.deleteOne(lamparaToDocument(unaLampara));
    }

    public void borrarFabricante(Fabricante unFabricante) {
        coleccionfabricantes.deleteOne(fabricanteToDocument(unFabricante));

    }
}
