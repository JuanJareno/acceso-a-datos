package com.practicaUD4.base;

import org.bson.types.ObjectId;

import java.time.LocalDate;

public class Lampara {
    //Campos
    private ObjectId id;
    private String nombre;
    private String marca;
    private double precio;
    private LocalDate fechaCompra;

    public Lampara() {

    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }


    public LocalDate getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(LocalDate fechaCompra) {
        this.fechaCompra = fechaCompra;
    }


    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }


    @Override
    public String toString() {
        return nombre + ": " + marca ;
    }
}
